# Freedom RP (Alt:V)
## Основа

За основу проекту был взят уже практически готовый проект от Stuyk [отсюда](https://github.com/Stuyk/openrp-altv). Но, насколько, мне на даный момент известно, этот проект еще нигде не светился, что может значить только о его неоконченности. К сожалению, большое количество вещей сейчас придеться переделывать и менять, чем и будем заниматься в ближайшее время.

## Установка

Сервер будет раюотать на Убунту 18+, как и проситься от разработчика Alt:V

**Документация по Alt:V**

- [Клиент](https://docs.altv.mp/js/api/alt-client.html)
- [Сервер](https://docs.altv.mp/js/api/alt-server.html)

**Зависимости:**

-   [NodeJS v.13+](https://nodejs.org/en/)

-   [GIT](https://git-scm.com/downloads)

-   [Mongo](https://docs.mongodb.com/manual/installation/)

-   Join your server with Discord Open.

-   **IF DISCORD DOES NOT PROMPT YOU** Copy the code on screen.

-   PM the Bot you setup earlier with `!login <code>`