import dotenv from 'dotenv'
dotenv.config()

import { existsSync, writeFile, copyFile } from 'fs';
import { join } from 'path';
import { exec } from 'child_process';

import path from 'path';
const __dirname = path.resolve();

const platform = process.platform === 'win32' ? 'windows' : 'linux';



async function startup() {

    // TODO: Connect to mongo db
    // TOOD: Connect Discord

    const newDiscordInfo = 'Setup new Discord information for Login Data? (y/n). Default is y\r\n';

        console.log('!!! IMPORTANT INFORMATION FOR DISCORD SETUP !!!');
        console.log('Please Create a Discord Application for your Login System.');
        console.log('Please Visit: https://discordapp.com/developers/applications/');
        console.log('1. Hit New Application');
        console.log('2. Set the Name for Your Bot / Application');
        console.log('3. Click on the `Bot` tab.');
        console.log('4. Transform your Application into a bot.');
        console.log('5. Name your bot.');
        console.log('6. Tick `Administrator` or just `Send/Read Messages`');
        console.log('7. Copy the bots secret token.');
        console.log('8. Make sure the bot is not public.');
        console.log('9. Navigate to oAuth2 tab. Tick `bot` in scopes.');
        console.log('10. Copy the URL inside of scopes. Paste in browser.');
        console.log('11. Add the bot to your designated Discord.');

        const discordPublicURLQ =
            '\r\nPlease enter a public discord url to display to users so they can join. \r\n';
        res = await question(discordPublicURLQ);

        if (res) {
            discordAppInfo.discord = res;
            res = undefined;
        }

        const discordConfigPath = join(
            __dirname,
            '/resources/orp/server/discord/configuration.json'
        );

        await new Promise(resolve => {
            writeFile(
                discordConfigPath,
                JSON.stringify(discordAppInfo, null, '\t'),
                () => {
                    console.log('\r\nDiscord Login Configured\r\n');
                    resolve();
                }
            );
        });
    }

    console.log(`Downloading Latest alt:V Server Files.`);
    const q6 = '\r\nWhich alt:V Branch? 0: Release, 1: RC, 2: Dev [Default: Release]\r\n';
    res = await question(q6);

    if (!res) {
        res = 0;
    }

    if (parseInt(res) === 0) {
        if (platform === 'windows') {
            console.log('Windows');
            windowsURLS.forEach(res => {
                res.url = res.url.replace('beta', 'release');
            });
            console.log('You have selected the RELEASE branch.');
        } else {
            console.log('Linux');
            linuxURLS.forEach(res => {
                res.url = res.url.replace('beta', 'release');
            });
            console.log('You have selected the RELEASE branch.');
        }
    }

    if (platform !== 'windows') {
        exec('chmod +x ./start.sh', (err, stdout, stderr) => {
            if (err) {
                console.log(err);
                return;
            }
        });

        exec('chmod +x ./altv-server', (err, stdout, stderr) => {
            if (err) {
                console.log(err);
                return;
            }
        });
    }

    process.exit(0);
}

startup();
