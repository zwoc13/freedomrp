import { Entity, ObjectIdColumn, ObjectID, Column } from 'typeorm';

// These are the different table schemas provided.
// Account is required for the login system.
@Entity()
export class Account {

  @ObjectIdColumn()
  id: ObjectID

  @Column()
  userId: string
  
  @Column()
  rank: number

  @Column()
  email: string
}

@Entity()
export class CharacterAppearance {
  
  @Column()
  sex: string

  @Column()
  face: string

  @Column()
  structure: string

  @Column()
  hair: string

  @Column()
  eyes: string

  @Column()
  detail: string

  @Column()
  makeup: string 
  
  @Column()
  tattoo: string
}

@Entity()
export class CharacterSkills {
  @Column(0)
  agility: number

  @Column(0)
  cooking: number

  @Column(0)
  crafting: number

  @Column(0)
  fishing: number

  @Column(0)
  gathering: number

  @Column(0)
  mechanic: number

  @Column(0)
  medicine: number

  @Column(0)
  mining: number

  @Column(0)
  smithing: number

  @Column(0)
  woodcutting: number
}

@Entity()
export class Character {
  
  @ObjectIdColumn()
  id: ObjectID

  @Column()
  guid: string

  @Column()
  name: string

  @Column()
  creation: object

  @Column()
  lastLogin: object

  @Column()
  playingTime: number

  @Column(type => CharacterAppearance)
  appearance: CharacterAppearance

  @Column()
  inventory: []

  @Column()
  equipment: []

  @Column()
  lastPosition: string

  @Column(200)
  health: number

  @Column(0)
  armor: number

  @Column(0)
  cash: number

  @Column(false)
  dead: boolean

  @Column(type => CharacterSkills)
  skills: CharacterSkills

  @Column()
  contacts: []

  @Column(-1)
  arrestTime: number

  @Column(0)
  extraVehicleSlots: number

  @Column(0)
  extraHouseSlots: number

  @Column(0)
  extraBusinessSlots: number

  @Column()
  dimension: number

  @Column(type => Faction)
  faction: Faction
}

@Entity()
export class FactionMember {
  
  @Column()
  guid: number

}

// TODO: Create Turf entity
@Entity()
export class Turf {

  @ObjectIdColumn()
  id: ObjectID

  @Column()
  x: number

  @Column()
  y: number

  @Column()
  z: number
}

@Entity()
export class Factions {
  
  @ObjectIdColumn()
  id: ObjectID

  @Column()
  creation: date

  @Column()
  name: string

  @Column(type => FactionMember)
  members: FactionMember[]

  @Column()
  ranks: string

  @Column()
  turfs: Turf[]

  @Column(1)
  color: number

  @Column()
  notice: string

  @Column()
  home: string

  @Column()
  vehiclepoints: string[]

  @Column(1)
  subtype: number

  @Column()
  unlocks: object

  @Column(0)
  unlocked: number

  @Column(0)
  vehiclesAvailable: number

  @Column(0)
  aircraftAvailable: number
}

@Entity()
export class Rotation {
  @Column()
  x: number

  @Column()
  y: number

  @Column()
  z: number
}

@Entity()
export class VehicleStats {
  @Column(1000)
  health: number

  @Column(1000)
  engine: number

  @Column()
  locked: boolean
}

@Entity()
export class Vehicle {
  @ObjectIdColumn()
  id: ObjectID

  @Column()
  guid: number

  @Column()
  model: string

  @Column()
  position: string

  @Column(type => Rotation)
  rotation: Rotation

  @Column(type => VehicleStats)
  stats: VehicleStats
  
  @Column()
  customization: object

  @Column(100)
  fuel: number
  
  @Column()
  inventory: []
}

// TODO: Check if we need this Details Entity? 

@Entity()
export class Details {
  @ObjectIdColumn()
  id: ObjectID

  @Column()
  mdc: string
}

// TODO: Check if we need this doors? 

@Entity()
export class Door {
  @ObjectIdColumn()
  id: ObjectID

  @Column()
  guid: number

  @Column()
  locked: boolean

  @Column()
  salePrice: number
}